﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class GameController : MonoBehaviour
{
    public EnemyGenerator enemyGenerator;
    public GameObject player, leftBorder, rightBorder, downBorder, upBorder;

    //UI
    public GameObject gamePanel, losePanel;
    public Text scoreText, bestResultText;

    //Locals
    public List<GameObject> tempParticles, rockets;
    Vector3 playerStartPos;
    int score;

    void Awake()
    {
        //Initializing variables
        playerStartPos = player.transform.position;
        scoreText.gameObject.SetActive(false);
        score = 0;
        tempParticles = new List<GameObject>();
        
        //Calculating positions of screen borders
        var dist = (transform.position - Camera.main.transform.position).z;
        Vector2 leftBorderPos = new Vector2(Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).x, Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).y);
        Vector2 rightBorderPos = new Vector2(Camera.main.ViewportToWorldPoint(new Vector3(1, 1, dist)).x, Camera.main.ViewportToWorldPoint(new Vector3(1, 1, dist)).y);

        //Moving borders to destroy inaccessible objects
        leftBorder.transform.position = new Vector3(leftBorderPos.x * 1f, 0);
        rightBorder.transform.position = new Vector3(rightBorderPos.x * 1f, 0);
        downBorder.transform.position = new Vector3(0, leftBorderPos.y);
        upBorder.transform.position = new Vector3(0, rightBorderPos.y);
    }
    
    void Start()
    {

    }
    
    void Update()
    {

    }

    public void StartButtonClick()
    {
        score = 0;
        scoreText.text = "Score: 0";
        bestResultText.text = "";
        scoreText.gameObject.SetActive(true);
        player.GetComponent<PlayerController>().ResetValues();
        player.transform.position = playerStartPos;
        player.SetActive(true);
        enemyGenerator.gameObject.SetActive(true);
        gamePanel.SetActive(true);
        losePanel.SetActive(false);
        enemyGenerator.StartGenerator();
    }

    public void ResetButtonClick()
    {
        PlayerPrefs.DeleteAll();
        bestResultText.text = "";
        scoreText.gameObject.SetActive(false);
    }

    //Adding score for killed enemies
    public void AddScore(int newScore)
    {
        score += newScore;
        scoreText.text = "Score: " + score;
    }

    //Explosion particles of rocket
    public void DoExplosion(GameObject prefab, Vector3 rocketPlace)
    {
        UpdateLists();
        GameObject explosion = Instantiate(prefab, rocketPlace, Quaternion.Euler(Vector3.zero));
        tempParticles.Add(explosion);
        StartCoroutine(ExplosionDestroyer(explosion));
    }

    //Destroying particles that have been showed
    IEnumerator ExplosionDestroyer(GameObject explosionParticle)
    {
        yield return new WaitForSeconds(1);
        Destroy(explosionParticle);
        yield return new WaitForEndOfFrame();
    }

    public void GameOver()
    {
        player.SetActive(false);
        enemyGenerator.gameObject.SetActive(false);
        enemyGenerator.ClearAll();
        ClearLists();
        if (score > PrefsManager.Record)
        {
            PrefsManager.Record = score;
        }
        bestResultText.text = "BEST RESULT:\n" + PrefsManager.Record.ToString();
        gamePanel.SetActive(false);
        losePanel.SetActive(true);
    }

    //Updating and cleaning lists with temporary particles and rockets objects 
    void UpdateLists()
    {
        tempParticles.RemoveAll(item => item == null);
        rockets.RemoveAll(item => item == null);
    }

    void ClearLists()
    {
        foreach (var t in tempParticles)
        {
            Destroy(t);
        }
        foreach (var r in rockets)
        {
            Destroy(r);
        }
        tempParticles.RemoveAll(item => item == null);
        rockets.RemoveAll(item => item == null);
    }
}
