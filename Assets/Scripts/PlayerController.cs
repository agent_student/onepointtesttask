﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public GameController gameController;
    public GameObject[] lives;
    public GameObject rocketPrefab;
    public float moveSpeed, reloadTime, rocketSpeed;

    //Locals
    float verticalMove, horizontalMove;
    int lifeCount;
    bool reload;
    Rigidbody2D rbPlayer;

    private void Awake()
    {
        reload = false;
        rbPlayer = gameObject.GetComponent<Rigidbody2D>();
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }
    
    void Start()
    {

    }
    
    void Update()
    {
        if (Input.GetButtonDown("Jump") && !reload)
        {
            StartCoroutine(FireCoroutine());
        }
    }

    //Rocket generator
    IEnumerator FireCoroutine()
    {
        reload = true;
        GameObject rocket = Instantiate(rocketPrefab, gameObject.transform.position + new Vector3(1, 0, 0), Quaternion.Euler(new Vector3(0, 0, 270)));
        rocket.tag = "PlayerRocket";
        rocket.GetComponent<Rigidbody2D>().velocity = Vector2.right * moveSpeed;
        gameController.rockets.Add(rocket);
        yield return new WaitForSeconds(reloadTime);
        reload = false;
        yield return new WaitForEndOfFrame();
    }

    //Player movement control
    private void FixedUpdate()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal") * moveSpeed;
        verticalMove = Input.GetAxisRaw("Vertical") * moveSpeed;

        Vector2 movement = new Vector2(horizontalMove, verticalMove);
        rbPlayer.velocity = movement;

        rbPlayer.position = new Vector3(Mathf.Clamp(rbPlayer.position.x, gameController.leftBorder.transform.position.x, gameController.rightBorder.transform.position.x),Mathf.Clamp(rbPlayer.position.y, gameController.downBorder.transform.position.y, gameController.upBorder.transform.position.y),0);
    }

    //Decrement lives when collided with Enemy or its rocket
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "EnemyRocket" || collider.gameObject.tag == "Enemy")
        {
            lifeCount--;
            if (lifeCount <= 0)
            {
                gameController.GameOver();
            }
            else
            {
                lives[lifeCount].SetActive(false);
                Destroy(collider.gameObject);
            }
        }
    }

    public void ResetValues()
    {
        lifeCount = 3;
        foreach (var life in lives)
        {
            life.SetActive(true);
        }
    }
}
