﻿using System.Collections.Generic;
using UnityEngine;

public static class
PrefsManager
{
    public static int Record
    {
        get { return PlayerPrefs.GetInt("record", 0); }
        set { PlayerPrefs.SetInt("record", value); PlayerPrefs.Save(); }
    }
}