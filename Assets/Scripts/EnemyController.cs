﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyController : MonoBehaviour
{
    public GameObject[] rocketSpawnPositions;
    public GameObject rocketPrefab;
    public GameObject deathPrefab;

    public GameController gameController;

    //Settings of enemy
    public float moveSpeed, rocketSpeed;
    public float shootDelay;
    public int scoreValue;
    public int lifeCount;
    SpriteRenderer enemyColor;

    private void Awake()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        this.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.left;
        enemyColor = gameObject.GetComponent<SpriteRenderer>();
        StartCoroutine(FireCoroutine());
    }
    
    void Start()
    {

    }
    
    void Update()
    {

    }

    //Changing color then enemy damaged
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.gameObject.tag == "Player")
        {
            gameController.DoExplosion(deathPrefab, gameObject.transform.position);
            gameController.AddScore(scoreValue);
            Destroy(this.gameObject);
        }
        else if(collider.gameObject.tag == "PlayerRocket")
        {
            lifeCount--;
            if (lifeCount == 2)
            {
                enemyColor.color = Color.blue;
            }
            else if (lifeCount == 1)
            {
                enemyColor.color = Color.red;
            }
            else if (lifeCount <= 0)
            {
                gameController.DoExplosion(deathPrefab, gameObject.transform.position);
                gameController.AddScore(scoreValue);
                Destroy(this.gameObject);
            }
        }
    }

    //Enemy shooting
    IEnumerator FireCoroutine()
    {
        while (true)
        {
            foreach (var s in rocketSpawnPositions)
            {
                GameObject rocket = Instantiate(rocketPrefab, s.transform.position, Quaternion.Euler(new Vector3(0, 0, 90)));
                rocket.tag = "EnemyRocket";
                rocket.GetComponent<Rigidbody2D>().velocity = Vector2.left * rocketSpeed;
                gameController.rockets.Add(rocket);
            }
            yield return new WaitForSeconds(shootDelay);
        }
    }
}
