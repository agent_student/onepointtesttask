﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ISpaceShip {

    public virtual void Destroy(GameObject ship)
    {
        Destroy(ship);
    }
}
