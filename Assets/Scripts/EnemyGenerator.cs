﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
    //Enemy Prefabs and Lists
    public GameObject[] enemyPrefabs;
    public List<GameObject> enemies;

    public float enemyDelay;
    public bool generate = false;

    private void Awake()
    {
        enemyDelay = 3;
        enemies = new List<GameObject>();
    }

    void Start()
    {

    }

    void Update()
    {
        if (generate)
        {
            StartCoroutine(EnemyGeneration());
        }
    }

    //Enemy generator coroutine
    public IEnumerator EnemyGeneration()
    {
        generate = false;
        enemies.RemoveAll(item => item == null);
        enemies.Add(Instantiate(enemyPrefabs[Random.Range(0, 2)], Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Random.Range(Screen.height / 10, Screen.height * 0.9f), 10)), Quaternion.Euler(0, 0, 90)));
        yield return new WaitForSeconds(Random.Range(enemyDelay, 1.5f * enemyDelay));
        generate = true;
        yield return new WaitForEndOfFrame();
    }

    public void StartGenerator()
    {
        generate = true;
        StartCoroutine(EnemyGeneration());
    }

    //Stop Generation
    public void ClearAll()
    {
        generate = false;
        foreach (var e in enemies)
        {
            Destroy(e);
        }
        enemies.RemoveAll(item => item == null);
    }
}
