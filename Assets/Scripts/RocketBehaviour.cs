﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketBehaviour : MonoBehaviour
{
    //Particles Prefabs
    public GameObject collisionRocket, collisionShip;

    public GameController gameController;

    private void Awake()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }
    
    void Start()
    {

    }
    
    void Update()
    {

    }

    //Different particles depending on collided object
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag != gameObject.tag)
        {
            if ((collider.gameObject.tag.Contains("Rocket")))
            {
                gameController.DoExplosion(collisionRocket, gameObject.transform.position);
                Destroy(this.gameObject);
            }
            else if (collider.gameObject.tag == "Finish")
            {
                Destroy(this.gameObject);
            }
            else if (!gameObject.tag.Contains(collider.gameObject.tag))
            {
                gameController.DoExplosion(collisionShip, gameObject.transform.position);
                Destroy(this.gameObject);
            }
        }
    }
}
